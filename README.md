# MiniGIS

The master's students of the program Information System Technologies at France's National School of Geographical Sciences - ENSG, were invited to take part in a software development project. The project consists of developing a 2D/3D GIS using C++, Qt and OpenGL.

## Description
MiniGIS is a GIS Desktop application capable of displaying, manipulating, and analyzing the various forms of spatial data. MiniGIS supports Vector and Raster data format. The data represented in the application will come from web feeds, existing databases, or files from local or remote repositories.

## Visuals

..upcoming

## Installation

The application should run under other OS, but we **highly** recommend you to use Linux.

Check if the `ubuntugis` repository is installed:
```
ls -l /etc/apt/sources.list.d/ | grep gis
```

If not, install it with the command:
```
add-apt-repository ppa:ubuntugis/ppa
```

Please install dependecies:
```
apt-get update -y && apt-get -y --no-install-recommends install \
    build-essential \
    libgtest-dev \
    clang \
    cmake \
    gdb \
    wget \
    graphviz \
    doxygen \
    doxygen-gui \
    gdal-bin \
    libgdal-dev \
    libglm-dev \
    libglew-dev \
    libcityglm-dev \
    libglfw3-dev
```

Clone this repository on your computer.
```
git clone https://gitlab.com/Jackgeo/MiniGIS.git
```

For some reason, building the project with a typical `cmake ..` / `make` fails due to QWidgets. We recommend you to install QTCreator instead (see https://web.stanford.edu/dept/cs_edu/resources/qt/install-linux) and run the project in this environment.

## Resources

Project's Microsoft Teams : https://ensgeu.sharepoint.com/:f:/s/PromoTSI22/Eudc1LsT9vJLkox-3eHH5oQBs9uZAZCvdyxpwZuB03wmFw?e=TfAZnL

Analysis Report : https://fr.overleaf.com/2258889628tjdjgwhjzcsn 

Final Report : https://fr.overleaf.com/6662893524mbwmwhwpsspt

Final Presentation : https://docs.google.com/presentation/d/1i5TLsNFDMSeKY6OIlEp2zrOz4641jIrW5x5KSl-erU4/edit#slide=id.gc6f980f91_0_37

## Support
tsi22@ensg.eu

## Author

Students of 2023, Master TSI, Information System Technologies. 
France's National School of Geographical Sciences, ENSG.

## Project status
Ongoing -> Development will cease December  15th 2022.
