#include <gtest/gtest.h>
#include "../../../include/Back/project.h"
#include "../../../include/Back/wfs.h"
#include "../../../include/Back/vectorGeometry.h"

using namespace std;

/*
TEST(ProjectLoadSHPTest, SmoothArgs) {
    string parentDir = "/home/formation/Bureau/DD/MiniGIS/MiniGIS/test/Back/data/jsonTest";
    string projectPath= parentDir + "/reloadtest.json";

    Project ourProject(projectPath.c_str());

    EXPECT_EQ(ourProject.getName(), string("my favorite project2"))<<"name is wrong";
    EXPECT_EQ(ourProject.getProjection(), 2154)<<"projection is wrong";
}

TEST(ProjectLoadSHPTest, WrongPath) {
    string parentDir = "/home/formation/Bureau/DD/MiniGIS/MiniGIS/test/Back/data/jsonTest";
    string projectPath= parentDir + "/test2";

    ASSERT_ANY_THROW(Project ourProject(projectPath.c_str()));
}

TEST(ProjectLoadSHPTest, NoPath) {
    string projectPath="";

    ASSERT_ANY_THROW(Project ourProject(projectPath.c_str()));
}

TEST(ProjectLoadSHPTest, WrongJson) {
    string parentDir = "/home/formation/Bureau/DD/MiniGIS/MiniGIS/test/Back/data/jsonTest";
    string projectPath= parentDir + "/WrongJSON.json";

    ASSERT_ANY_THROW(Project ourProject(projectPath.c_str()));
}


TEST(ProjectSaveSHPTest, NoArgs) {
    string parentDir = "/home/formation/Bureau/DD/MiniGIS/MiniGIS/test/Back/data/jsonTest";
    string projectPath= parentDir + "/reloadtest.json";

    Project ourProject(projectPath.c_str());
    ourProject.LayerByID(1)->setPath(string("jajaja"));
    EXPECT_EQ(ourProject.LayerByID(1)->getPath(), string("jajaja"));
    ourProject.save();

    Project ourProject2(projectPath.c_str());
    EXPECT_EQ(ourProject2.LayerByID(1)->getPath(), string("jajaja"));
}

TEST(ProjectSaveSHPTest, CorrectPath)
{
    string parentDir = "/home/formation/Bureau/DD/MiniGIS/MiniGIS/test/Back/data/jsonTest";
    string projectPath = parentDir + "/reloadtest.json";
    string savePath = parentDir + "/ProjectSaveSHPTest_Correctpath.json";

    Project ourProject(projectPath.c_str());
    ourProject.save(savePath.c_str());

    Project ourProject2(savePath.c_str());
    EXPECT_STREQ(ourProject2.getPath(), savePath.c_str());
}*/

TEST(ManipulatingGMLFile, CorrectPath)
{
    // arrange
    string gmlPath = "../data/gml/08_PASSERELLE_HOMME_ROCHE.gml";
    string gmlName = "gmlname";
    Project ourProject;

    // act
    uint ind = ourProject.addLayerGML(gmlPath, gmlName);

    // assert
    EXPECT_EQ(ind, 1);
}

TEST(ManipulatingGMLFile, UnvalidPathGMLext)
{
    // arrange
    string gmlPath = "bad/path.gml";
    string gmlName = "gmlname";
    Project ourProject;

    // act and assert
    ASSERT_ANY_THROW(uint ind = ourProject.addLayerGML(gmlPath, gmlName));
}

TEST(ManipulatingGMLFile, computingGeometry)
{
    // arrange
    string gmlPath = "../data/gml/08_PASSERELLE_HOMME_ROCHE.gml";
    string gmlName = "gmlname";
    Project ourProject;
    uint ind = ourProject.addLayerGML(gmlPath, gmlName);
    Layer *layer = ourProject.LayerByID(ind);
    LayerGML gmllayer(layer->getPath(), layer->getName());

    // act
    LayerFeatures leayerfeatures = gmllayer.getLayerFeatures();

    // assert
    EXPECT_EQ(leayerfeatures.features.size(),3);
}

TEST(ManipulatingGMLFile, getboundingbox)
{
    // arrange
    string gmlPath = "../data/gml/08_PASSERELLE_HOMME_ROCHE.gml";
    string gmlName = "gmlname";
    Project ourProject;
    uint ind = ourProject.addLayerGML(gmlPath, gmlName);
    Layer *layer = ourProject.LayerByID(ind);
    LayerGML gmllayer(layer->getPath(), layer->getName());

    // act
    vector<float> bbx = gmllayer.getBoundingBox();

    // assert
    EXPECT_TRUE(1842660 < bbx[0] < 1842661);
}

// Tests de la classe wfs
TEST(WfsFetchWfs, WrongPath)
{
    string filePath  = "http:";
    string layerName = "FOR_PUBL_FR";

    ASSERT_ANY_THROW(Wfs wfs(filePath, layerName));
}

TEST(WfsFetchWfs, NoPath)
{
    string filePath="";
    string layerName = "FOR_PUBL_FR";

    ASSERT_ANY_THROW(Wfs wfs(filePath, layerName));
}

TEST(WfsGetProjection, CorrectProjection)
{
    string filePath = "http://ws.carmencarto.fr/WFS/105/ONF_Forets";
    string layerName = "FOR_PUBL_FR";

    Wfs wfs(filePath, layerName);
    int projectionNumber = wfs.GetProjection("ATE_FR");

    EXPECT_EQ(projectionNumber, 4171)<<" The returned projection is: "<<projectionNumber<<".";
}

TEST(WfsFoundLayerNames, CorrectNumberOfFields)
{
    string filePath = "http://ws.carmencarto.fr/WFS/105/ONF_Forets";
    string layerName = "FOR_PUBL_FR";

    Wfs wfs(filePath, layerName);
    vector<string> fieldNames = wfs.getLayerNames();

    EXPECT_EQ(fieldNames.size(), 6)<<" The function found "<<fieldNames.size()<<" layer.";
}

TEST(WfsFoundLayerNames, CorrectNamesOfFields)
{
    string filePath = "http://ws.carmencarto.fr/WFS/105/ONF_Forets";
    string layerName = "FOR_PUBL_FR";

    Wfs wfs(filePath, layerName);
    vector<string> fieldNames = wfs.getLayerNames();
    vector<string> realNames = {"ATE_FR", "DT_FR", "FOR_PUBL_FR", "PARC_PUBL_FR", "PRS_FR", "RB_FR"};

    for (string name : fieldNames){
        EXPECT_EQ(count(realNames.begin(), realNames.end(), name), 1)<<" The name "<<name<<" was not found.";
    }
}

TEST(WfsComputeGeometries, CorrectNumberOfFeatures)
{
    string filePath = "http://ws.carmencarto.fr/WFS/105/ONF_Forets";
    string layerName = "FOR_PUBL_FR";

    Wfs wfs(filePath, layerName);

    EXPECT_EQ(wfs.getLayerFeatures().features.size(), 17012)<<" There is: "<<wfs.getLayerFeatures().features.size()<<" features.";
}

TEST(WfsComputeGeometries, CorrectType)
{
    string filePath = "http://ws.carmencarto.fr/WFS/105/ONF_Forets";
    string layerName = "FOR_PUBL_FR";

    Wfs wfs(filePath, layerName);

    VectorGeometry vectorGeometry;

    EXPECT_EQ(wfs.getLayerFeatures().features[0].type, Type::POLYGON)<<" The returned type is: "<<vectorGeometry.typeString(wfs.getLayerFeatures().features[0].type)<<".";
}

TEST(LayerVectorComputeGeometries, CorrectType)
{
    string filePath = "http://ws.carmencarto.fr/WFS/105/ONF_Forets";
    string layerName = "FOR_PUBL_FR";

    Wfs wfs(filePath, layerName);

    VectorGeometry vectorGeometry;

    EXPECT_EQ(wfs.getLayerFeatures().features[0].type, Type::POLYGON)<<" The returned type is: "<<vectorGeometry.typeString(wfs.getLayerFeatures().features[0].type)<<".";
}
/*
// Test de la classe vectorGeometry
TEST(VectorGeometryTypeString, CorrectString)
{
    VectorGeometry vectorGeometry;

    std::string resPoint = vectorGeometry.typeString(Type::POINT);
    std::string resLine = vectorGeometry.typeString(Type::LINE);
    std::string resPolygon = vectorGeometry.typeString(Type::POLYGON);
    std::string resNull = vectorGeometry.typeString(Type::NULLTYPE);;

    EXPECT_EQ(resPoint, "Point")<<" The function returned: "<<resPoint<<".";
    EXPECT_EQ(resLine, "Line")<<" The function returned: "<<resLine<<".";
    EXPECT_EQ(resPolygon, "Polygon")<<" The function returned: "<<resPolygon<<".";
    EXPECT_EQ(resNull, "NULLTYPE")<<" The function returned: "<<resNull<<".";
}

TEST(VectorGeometryCalculatePointVertex, CorrectGeometry)
{
    VectorGeometry vectorGeometry;

    OGRGeometry* geometry = {};

    std::vector<glm::vec3> geometries = vectorGeometry.calculatePointVertex(geometry);

    EXPECT_EQ(geometries, "Point");
}

TEST(ProjectgetAttributesIDTEST,Smooth)
{
    string parentDir = "/home/samuel/Documents/qtcreator/data/shp";
    string layerPath = parentDir + "/linestring.shp";
    long IID=1;

    string layertest="linestring";

    Project ourProject;
    ourProject.addLayerSHP(layerPath ,layertest);

    Layer* polayer= ourProject.LayerByID(1);
    LayerVector* polayerbis=(LayerVector* ) polayer;


    LayerVector* polayerbis2=(LayerVector* ) polayer;



    EXPECT_EQ(polayerbis->getAttributesID(IID), polayerbis2->getAttributesID(1));
}

TEST(ProjectgetAttributesTEST,Smooth)
{
    string parentDir = "/home/samuel/Documents/qtcreator/data/shp";
    string layerPath = parentDir + "/linestring.shp";

    string layertest= "linestring";


    Project ourProject;
    ourProject.addLayerSHP(layerPath ,layertest);

    Layer* polayer= ourProject.LayerByID(1);
    LayerVector* polayerbis=(LayerVector* ) polayer;



    EXPECT_EQ(polayerbis->getAttributes()[2][2], "trait"  );
}


TEST(ProjectgetFieldNamesTEST,Smooth)
{
    string parentDir = "/home/samuel/Documents/qtcreator/data/shp";
    string layerPath = parentDir + "/linestring.shp";

    string layertest= "linestring";


    Project ourProject;
    ourProject.addLayerSHP(layerPath ,layertest);

    Layer* polayer= ourProject.LayerByID(1);
    LayerVector* polayerbis=(LayerVector* ) polayer;



    EXPECT_EQ(polayerbis->getFieldNames()[1], "TYPE"  );
}*/



