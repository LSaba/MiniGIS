#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <iostream>
#include <cstring>
#include<QMessageBox>
#include <string>

#include "QKeyEvent"
#include "QMessageBox"
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "displaypanel.h"

MainWindow * MainWindow::pMainWindow = nullptr;

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow), panel(new LayersPanel), project(new Project) {
    ui->setupUi(this);
    pMainWindow = this;
    //zoom buttons connection
    QObject::connect(ui->actionZoom, SIGNAL(triggered()), this, SLOT(zoomplus()));
    QObject::connect(ui->actionZoomMinus, SIGNAL(triggered()), this, SLOT(zoomminus()));

    //2d/3d mode button connection
    QObject::connect(ui->actionxdButton, SIGNAL(triggered(bool)), this, SLOT(changeXdMode(bool)));

    //layers buttons connection
    QObject::connect(ui->LinkAddVector, SIGNAL(triggered(bool)), this, SLOT(addVectorPath()));
    QObject::connect(ui->LinkAddRaster, SIGNAL(triggered(bool)), this, SLOT(addRasterPath()));

    QObject::connect(ui->LinkAddMNT, SIGNAL(triggered(bool)), this, SLOT(addMNTPath()));
    QObject::connect(ui->actionSave, SIGNAL(triggered(bool)), this, SLOT(saveProject()));
    QObject::connect(ui->actionSaveAs, SIGNAL(triggered(bool)), this, SLOT(saveAsProject()));
    QObject::connect(ui->actionLoad, SIGNAL(triggered(bool)), this, SLOT(loadProject()));
    QObject::connect(ui->LinkAddWFS, SIGNAL(triggered(bool)), this, SLOT(addWFSPath()));
    QObject::connect(ui->LinkAddWMS, SIGNAL(triggered(bool)), this, SLOT(addWMSPath()));
    QObject::connect(ui->LinkAddCityGML, SIGNAL(triggered(bool)), this, SLOT(addCityGMLPath()));

    // Keyboard shortcut for Qt Action in menu
    ui->actionSave->setShortcutContext(Qt::ApplicationShortcut);
    ui->actionSaveAs->setShortcutContext(Qt::ApplicationShortcut);
    ui->actionLoad->setShortcutContext(Qt::ApplicationShortcut);

    ui->openGLWidget->setLayersPanel(panel);
    ui->openGLWidget->setProject(project);
    panel->container->setAlignment(Qt::AlignTop);


}

MainWindow::~MainWindow(){
    delete ui;
}

MainWindow *MainWindow::getMainWinPtr(){
    return pMainWindow;
}

void MainWindow::keyPressEvent(QKeyEvent *e){
    if (e->key() == Qt::Key_Escape){
        close();
    }
    else{
        QWidget::keyPressEvent(e);
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event){
    if (!(QApplication::keyboardModifiers() && Qt::ControlModifier)){
        getLayerinfo();
    }
}

bool MainWindow::eventFilter(QObject *o, QEvent *e) {
    QMenu *menu = new QMenu(this);
    QAction *d = new QAction("Remove layer", this);
    QAction *d1 = new QAction("Center to layer", this);
    d->setObjectName(o->objectName());
    d->setCheckable(true);
    d1->setObjectName(o->objectName());
    d1->setCheckable(true);
    QMouseEvent*mouseEvent = static_cast<QMouseEvent*>(e);
    QObject::connect(d1, SIGNAL(triggered(bool)), this, SLOT(centerLayer()));
    if (e->type()==QEvent::MouseButtonPress && mouseEvent->button()==Qt::RightButton) {

        menu->addAction(d);
        menu->addAction(d1);
        menu->exec(mouseEvent->globalPos());

        QCheckBox* checkbox = static_cast<QCheckBox *>(o);

        if (d->isChecked()) {
            std::string s =std::string("Do you want to delete the layer \"")+project->LayerByID(o->objectName().toUInt())->getName()+std::string("\" ?");
            switch( QMessageBox::question(
                        this,
                        tr("Delete the layer ?"),
                        tr(s.c_str()),
                        QMessageBox::Yes |
                        QMessageBox::Cancel,
                        QMessageBox::Cancel)) {
              case QMessageBox::Yes:
                project->removeLayer(o->objectName().toUInt());
                getLayerinfo();
                checkbox->setVisible(false);
                panel->container->removeWidget(checkbox);
                break;
              case QMessageBox::Cancel:
                break;
              default:
                break;
            }
        }
        return false;
    }
    menu = NULL;
    return QMainWindow::eventFilter(o,e);
}

void MainWindow::centerLayer(){
    QCheckBox* checkbox = (QCheckBox*) sender();
    ui->openGLWidget->centerLayer(checkbox->objectName().toUInt());
}

void MainWindow::zoomplus(){
    ui->openGLWidget->setZoomplus();
    getObjectinfo();
}

void MainWindow::zoomminus(){
    ui->openGLWidget->setZoomminus();
}

// Manage changements between 2d and 3d model
void MainWindow::changeXdMode(bool state){
    ui->openGLWidget->changeXdMode(state);
}

// Add vector data to the mainwindow
void MainWindow::addVectorPath() {
    setUpdatesEnabled(false);
    std::vector<QString> vec = panel->addVector(this, project);
    if(!vec[0].isEmpty()){
        QCheckBox *checkbox = new QCheckBox(vec[0], this);
        checkbox->setAccessibleName(vec[0]);
        checkbox->setObjectName(vec[1]);
        checkbox->setChecked(true);
        panel->container->addWidget(checkbox);
        ui->layersPanel->setLayout(panel->container);
        checkbox->installEventFilter(this);
        QObject::connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(visibleLayer(int)));

        getLayerinfo();
    }
    setUpdatesEnabled(true);
}

// Add raster data to the mainwindow
void MainWindow::addRasterPath(){
    setUpdatesEnabled(false);
    std::vector<QString> vec = panel->addRaster(this, project);
    if(!vec[0].isEmpty()){
        QCheckBox *checkbox = new QCheckBox(vec[0], this);
        checkbox->setAccessibleName(vec[0]);
        checkbox->setObjectName(vec[1]);
        checkbox->setChecked(true);
        panel->container->addWidget(checkbox);
        ui->layersPanel->setLayout(panel->container);
        checkbox->installEventFilter(this);

        QObject::connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(visibleLayer(int))); //TEST
         getLayerinfo();
    }
    setUpdatesEnabled(true);
}

void MainWindow::addMNTPath(){
    setUpdatesEnabled(false);
    std::vector<QString> vec = panel->addMNT(this, project);
    if(!vec[0].isEmpty()){
        QCheckBox *checkbox = new QCheckBox(vec[0], this);
        checkbox->setAccessibleName(vec[0]);
        checkbox->setObjectName(vec[1]);
        checkbox->setChecked(true);
        panel->container->addWidget(checkbox);
        ui->layersPanel->setLayout(panel->container);
        checkbox->installEventFilter(this);
        QObject::connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(visibleLayer(int))); //TEST
        getLayerinfo();
    }
    setUpdatesEnabled(true);
}


// Add WFS data to the mainwindow
void MainWindow::addWFSPath() {
    setUpdatesEnabled(false);
    try{
        std::vector<QString> vec = panel->addWFS(this, project);
        if(!vec[0].isEmpty()){
            if(!vec[1].isEmpty()){
                QCheckBox *checkbox = new QCheckBox(vec[0], this);
                checkbox->setAccessibleName(vec[0]);
                checkbox->setObjectName(vec[1]);
                checkbox->setChecked(true);
                panel->container->addWidget(checkbox);
                ui->layersPanel->setLayout(panel->container);
                checkbox->installEventFilter(this);
                QObject::connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(visibleLayer(int))); //TEST
                getLayerinfo();
                }
            }
        }catch(std::invalid_argument& e){
            std::cout<<e.what();
            QMessageBox messageBox;
            messageBox.critical(0,"Error","Provide a valid URL !");
            messageBox.setFixedSize(500,200);
        }catch(std::exception e){
            std::cout<<e.what();
            QMessageBox messageBox;
            messageBox.critical(0,"Error","Provide a valid URL !");
            messageBox.setFixedSize(500,200);
        }
    setUpdatesEnabled(true);
}

// Add cityGML data to the mainwindow
void MainWindow::addCityGMLPath() {
    setUpdatesEnabled(false);
    std::vector<QString> vec = panel->addCityGML(this, project);
        if(!vec[0].isEmpty()){
            QCheckBox *checkbox = new QCheckBox(vec[0], this);
            checkbox->setAccessibleName(vec[0]);
            checkbox->setObjectName(vec[1]);
            checkbox->setChecked(true);
            panel->container->addWidget(checkbox);
            ui->layersPanel->setLayout(panel->container);
            checkbox->installEventFilter(this);
            QObject::connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(visibleLayer(int)));
            getLayerinfo();
        }
    setUpdatesEnabled(true);
}


// Add WMS data to the mainwindow
void MainWindow::addWMSPath() {
    setUpdatesEnabled(false);
    try{
        std::vector<QString> vec = panel->addWMS(this, project);
        if(!vec[0].isEmpty()){
            QCheckBox *checkbox = new QCheckBox(vec[0], this);
            checkbox->setAccessibleName(vec[0]);
            checkbox->setObjectName(vec[1]);
            checkbox->setChecked(true);
            panel->container->addWidget(checkbox);
            ui->layersPanel->setLayout(panel->container);
            checkbox->installEventFilter(this);
            QObject::connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(visibleLayer(int)));
            getLayerinfo();
        }
    }
    catch(std::invalid_argument& e){
        std::cout<<e.what();
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Provide a valid URL !");
        messageBox.setFixedSize(500,200);
    }
    catch(std::exception e){
        std::cout<<e.what();
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Provide a valid URL !");
        messageBox.setFixedSize(500,200);
    }
    setUpdatesEnabled(true);
}

void MainWindow::visibleLayer(int i) {
    QCheckBox* checkbox = (QCheckBox*) sender();
    Layer* layer = project->LayerByID(checkbox->objectName().toUInt());
    layer->setSelected((bool) i);
    layer->setHidden(!(bool) i);
    getLayerinfo();
}

// Save project
void MainWindow::saveProject()
{
    // Retrieve the project's name and path
    std::string namePro = project->getName();
    std::string pathPro = project->getPath();

    if (namePro.length() == 0)
    {
        // The project is not saved already
        saveAsProject();
    }
    else{
        // The project is already saved
        project->save(pathPro);
    }
}


// Save As project
void MainWindow::saveAsProject(){
    setUpdatesEnabled(false);

    // Path where the project is saved by the user
    QString projectPath = QFileDialog::getSaveFileName(this,
                                                            QObject::tr("Saving project"),
                                                            "/home",
                                                            tr("JSON (*.json)"));

    QString proPath; // project's path with the .json extension

    // Check if projectPath has .json extension or not
    if(!projectPath.contains(".json", Qt::CaseInsensitive)){
        // Adding .json extension to projectPath
        QString extension{".json"};
        proPath.reserve(projectPath.length() + extension.length());
        proPath.append(projectPath);
        proPath.append(extension);
    }
    else{
        proPath = projectPath;
    }

    QString fileName = QFileInfo(proPath).baseName();

    // Check if a path is provided
    if(!projectPath.isEmpty()){
        std::string data = proPath.toStdString();
        std::string name = fileName.toStdString();
        // Setting the project's projection, name and path
        project->setProjection(2154);
        project->setName(name);
        project->setPath(data);
        }
    setUpdatesEnabled(true);
}




void MainWindow::getLayerinfo(){
    removetreeinfo();
    InfosFeaturesPanel infopanel;
     int size = infopanel.getvisiblelayer(project).size();
      //Layer layer = infopanel.getvisiblelayer(project).at(size - 1);

      for(int i=0;i<size;i++){
           Layer layer = infopanel.getvisiblelayer(project).at(i);
           // Create new item (top level item)
           QTreeWidgetItem *topLevelItem = new QTreeWidgetItem(ui->treeWidget);
           // Add it on our tree as the top item.

           ui->treeWidget->addTopLevelItem(topLevelItem);

           QString layername = QString::fromUtf8(layer.getName().c_str());
           QString layerpath = QString::fromUtf8(layer.getPath().c_str());
           QString layerprojection = QString::fromUtf8(layer.getProjection().c_str());

           topLevelItem->setText(0,layername);
           QTreeWidgetItem *itempath=new QTreeWidgetItem(topLevelItem);
           QTreeWidgetItem *itemprojection=new QTreeWidgetItem(topLevelItem);

           const char* path="File path";
           const char* projection="Projection";

           // Set text for item
           itempath->setText(0,path);
           itempath->setText(1,layerpath);
           itemprojection->setText(0,projection);
           itemprojection->setText(1,layerprojection);
      }
}

void MainWindow::loadProject() {
    setUpdatesEnabled(false);
    QString filePath = QFileDialog::getOpenFileName(this, QObject::tr("Open File"),"/home/",
                                                    QObject::tr("Couches (*.json)"));


    if (!filePath.isEmpty())
    {
        project->load(filePath.toStdString());
        for (int i=1; i<project->getLayers()->size()+1; i++) {
            QCheckBox *checkbox = new QCheckBox(QString::fromStdString(project->getLayers()->at(i).getName()), this);
            checkbox->setAccessibleName(QString::fromStdString(project->getLayers()->at(i).getName()));
            checkbox->setObjectName(QString::fromStdString(std::to_string(project->getLayers()->at(i).getId())));
            checkbox->setChecked(true);
            panel->container->addWidget(checkbox);
            ui->layersPanel->setLayout(panel->container);
            checkbox->installEventFilter(this);
            QObject::connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(visibleLayer(int)));
            getLayerinfo();
        }
    }
    setUpdatesEnabled(true);
}

void MainWindow::getObjectinfo(){
    //DisplayPanel displaypanel;
    removetreeinfo();
    std::vector<std::vector<string>> _objectinfo = ui->openGLWidget->getinfos;
    // Create new item (top level item)
    QTreeWidgetItem *topLevelItem = new QTreeWidgetItem(ui->treeWidget);
    // Add it on our tree as the top item.
    ui->treeWidget->addTopLevelItem(topLevelItem);
    ui->treeWidget->expandItem(topLevelItem);
    topLevelItem->setText(0,"infos");

     for(int i = 0;i<_objectinfo.size();i++){
         std::string attribut = _objectinfo.at(i).at(0);
         QString Qattribut = QString::fromUtf8(attribut.c_str());
         std::string value = _objectinfo.at(i).at(1);
         QString Qvalue = QString::fromUtf8(value.c_str());
         QTreeWidgetItem *item=new QTreeWidgetItem(topLevelItem);
         item->setText(0,Qattribut);
         item->setText(1,Qvalue);
     }
}

void MainWindow::removetreeinfo(){
   ui->treeWidget->clear();
}

