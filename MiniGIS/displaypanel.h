#ifndef DISPLAYPANEL_H
#define DISPLAYPANEL_H

#include <cmath>
#include <limits>
#include <QtWidgets>
#include <QtOpenGL>
#include <QOpenGLWidget>
#include <GL/glu.h>
#include <GL/gl.h>
#include "QWheelEvent"
#include "include/Back/layervector.h"
#include "include/Front/layerspanel.h"
#include "include/Back/layerraster.h"
#include "Front/infosfeaturespanel.h"
#include "Back/project.h"
#include "mainwindow.h"

/**
 * @brief The DisplayPanel class, implementing openGL functions.
 */

class DisplayPanel :public QOpenGLWidget {
    Q_OBJECT

    public:
        explicit DisplayPanel(QWidget *parent = 0);
        ~DisplayPanel();
        void setZoomplus();
        void setZoomminus();
        void setLayersPanel(LayersPanel* _layersPanel);
        void setProject( Project * _project);

        std::vector<std::vector<string>> getinfos;

        /**
         * @brief changeXdMode permits to go from 2D to 3D and back. It prevents rotations in 2D.
         * @param state is true if we're in 3D, false otherwise.
         */
        void changeXdMode(bool state);

        /**
         * @brief centerLayer centers the view on the given layer.
         * @param id of the layer.
         */
        void centerLayer(uint id);

    protected:

        /**
         * @brief zoom is the percentage of rescale of the figures.
         */
        float zoom = 100.0;

        /**
         * @brief xDep is the depth in the first coordinate.
         */
        float xDep = 0.0;

        /**
         * @brief yDep is the depth in the second coordinate.
         */
        float yDep = 0.0;

        /**
         * @brief ZDep has a misleading name, it is not the depth in the third coordinate, but is used to scale the lateral movements.
         */
        float ZDep = -3.0;
        std::vector<uint> Object_feat_ID;
        void mouseReleaseEvent(QMouseEvent *event);

        /**
         * @brief initializeGL creates the openGL widget.
         */
        void initializeGL();

        /**
         * @brief paintGL draws all layers according with the camera.
         */
        void paintGL();

        /**
         * @brief resizeGL initialize the camera and is called to resize the widget.
         * @param width of the widget.
         * @param height of the widget.
         */
        void resizeGL(int width, int height);

        /**
         * @brief wheelEvent zoom in or out when the wheel is scrolled.
         * @param event
         */
        void wheelEvent(QWheelEvent *event);

        /**
         * @brief mousePressEvent is necessary to the lateral movements. It gets the mouse last position.
         * @param event
         */
        void mousePressEvent(QMouseEvent *event);

        /**
         * @brief mouseMoveEvent permits lateral movements when the user clicks and drags his mouse.
         * @param event
         * @see mousePressEvent
         */
        void mouseMoveEvent(QMouseEvent *event);

    private:
        /**
         * @brief selected_object permits to pick an object. It returns true if an object is selected and changes the value of out_selected_feature_ID.
         * @param mouseX first coordinate of the mouse in pixels.
         * @param mouseY second coordinate of the mouse in pixels.
         * @param screenWidth width of the widget in pixels.
         * @param screenHeight height of the widget in pixels.
         * @param cam_pos contains 3 floats corresponding to the camera position.
         * @param cam_rot contains 3 floats corresponding to the camera rotation in radians.
         * @param out_selected_feature_ID if an object is clicked, it will be filled with the key of its layer and its id.
         * @return true if an object is clicked, false otherwise.
         * @see ScreenPosToWorldRay
         * @see TestRayOBBIntersection
         * @see rayBBIntersect
         */
        bool selected_object(int mouseX, int mouseY, int screenWidth, int screenHeight, glm::vec3 cam_pos, glm::vec3 cam_rot, std::vector<uint>* out_selected_feature_ID);

        /**
         * @brief draw is called in paintGL to create all the objects to paint.
         */
        void draw();

        /**
         * @brief lastPos is the last position of the mouse, used during lateral movements.
         */
        QPointF lastPos;
        LayersPanel* layersPanel;
        Project * project;

        /**
         * @brief xdMode is the state of the 2d/3d model (0 = 2d, 1 = 3d)
         */
        bool xdMode = 0;

        /**
         * @brief xRot is the rotation angle in degrees of the first axis.
         */
        float xRot;

        /**
         * @brief yRot is the rotation angle in degrees of the second axis.
         */
        float yRot;

        /**
         * @brief zRot is the rotation angle in degrees of the third axis.
         */
        float zRot;

        /**
         * @brief projMatrix is the projection matrix. It is needed in one version of the object selection (which doesn't work either).
         */
        glm::mat4 projMatrix;

        /**
         * @brief viewMatrix is the view matrix. It is needed in one version of the object selection (which doesn't work either).
         */
        glm::mat4 viewMatrix;

    public slots:
        void setXRotation(float angle);
        void setYRotation(float angle);
        void setZRotation(float angle);
         std::vector<std::vector<string>> getObjectClickedData();

    signals:
        void XRotationChanged(float angle);
        void YRotationChanged(float angle);
        void ZRotationChanged(float angle);

};

/**
 * @brief rayBBIntersect returns true if the ray and the bounding box given intersect and changes the value of dist to the distance between the ray origin and the object center.
 * @param ray_origin is the ray origin 3D coordinates.
 * @param ray_direction is the ray heading vector.
 * @param bb_min are the minimums coordinates of the bounding box in each axis.
 * @param bb_max are the maximums coordinates of the bounding box in each axis.
 * @param dist is filled with the distance to the object.
 * @return true if the ray and the bounding box intersect.
 */
bool rayBBIntersect(glm::vec3 ray_origin, glm::vec3 ray_direction, glm::vec3 bb_min, glm::vec3 bb_max, float& dist);

/**
 * @brief TestRayOBBIntersection returns true if the ray and the bounding box given intersect and changes the value of intersection_distance to the distance between the ray origin and the object center.
 * @param ray_origin is the ray origin 3D coordinates.
 * @param ray_direction is the ray heading vector.
 * @param aabb_min are the minimums coordinates of the bounding box in each axis.
 * @param aabb_max are the maximums coordinates of the bounding box in each axis.
 * @param ModelMatrix is the model matrix of the object (and thus, of the bounding box).
 * @param intersection_distance is filled with the distance to the object.
 * @return true if the ray and the bounding box intersect.
 */
bool TestRayOBBIntersection(glm::vec3 ray_origin,glm::vec3 ray_direction,glm::vec3 aabb_min,glm::vec3 aabb_max,glm::mat4 ModelMatrix,float& intersection_distance);

/**
 * @brief ScreenPosToWorldRay builds a ray: its origin and direction.
 * @param mouseX horizontal coordinates of the mouse in pixels.
 * @param mouseY vertical coordinates of the mouse in pixels.
 * @param screenWidth is the width of the widget in pixels.
 * @param screenHeight is the height of the widget in pixels.
 * @param ViewMatrix
 * @param ProjectionMatrix
 * @param out_origin will be set to the ray origin.
 * @param out_direction will be set to the ray direction.
 */
void ScreenPosToWorldRay(int mouseX, int mouseY,int screenWidth, int screenHeight, glm::mat4 ViewMatrix, glm::mat4 ProjectionMatrix, glm::vec3& out_origin, glm::vec3& out_direction);

/**
 * @brief middleCoord returns the middle of a 2D bounding box given.
 * @param bbox contains 4 floats: (x_min, x_max, y_min, y_max).
 * @return 2D coordinates of the middle of the bounding box.
 */
vector<float> middleCoord( vector<float> bbox);
#endif // DISPLAYPANEL_H
