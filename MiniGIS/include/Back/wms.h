#ifndef WMS_H
#define WMS_H

#include "gdal/gdal_priv.h"
#include "glm/glm.hpp"
#include <QImage>

#include "layer.h"


/**
 * !brief WMS is a Layer based on WMS data,
 * It is composed of Rasters with numerous bands
 */
class WMS : public Layer
{
private:

    /**
     * @brief Dataset used by OGR and GDAL.
     *
     */
    GDALDataset * WMSData;

    /**
     * @brief Complete URL of the WMS Request.
     *
     */
    std::string path;

    /**
     * @brief Name of WMS Layer.
     *
     */
    std::string layerName;

public:

    /**
     * @brief Constructor of WMS Class.
     *
     * @param url Complete URL of WMS Request
     * @param LayerName Name of WMS Layer.
     *
     * @return WMS Object
     *
     */
    WMS(std::string url, string LayerName);

    ~WMS();

    /**
     * @brief data that will be put in the vertexBuffer.
     *
     */
    std::vector<glm::vec3> dataWMS;

    /**
     * @brief Origins and Pixel sizes of the raster data.
     *
     */
    float Xo, Yo, Xsize, Ysize;

    /**
     * @brief The image that has been processed.
     *
     */
    QImage qtimage;

    /**
     * @brief data that will be put in the vertexBuffer
     *
     */

    /**
     * @brief list of indices in the raster image.
     *
     */
    std::vector<glm::vec2> indices;

    /**
     * @brief Computes the size of the WMS Data.
     *
     * @return Raster size and band count
     *
     */
    int* getWMSSize();

    /**
     * @brief Opens the WMS Data
     *
     * @param url Complete URL of WMS request.
     *
     */
    void openWMS(std::string url );

    /**
     * @brief Width of the data.
     *
     */
    int widthWMS;

    /**
     * @brief Height of the data.
     *
     */
    int heightWMS;

    /**
     * @brief Number of raster bands.
     *
     */
    int numberOfBands;

    /**
     * @brief Computes indices of the raster image.
     *
     * @param url Complete URL of WMS request.
     *
     */
    void createIndexTable(string url);

    /**
     * @brief Computes the data that will be inserted into the VertexBuffer.
     *
     * @param url Complete URL of WMS request.
     *
     */
    void fetchWMS(string url);

    /**
     * @brief Returns the origin of the raster data.
     *
     * @return origin of raster data.
     *
     */
    float* getWMSOrigin();

    /**
     * @brief Returns the size of a pixel of the image.
     *
     * @return size of a pixel.
     *
     */
    float* getPixelSize();

    /**
     * @brief Returns the projection in which the WMS data was given.
     *
     * @return projection of WMS.
     *
     */
    const char* getWMSProjection();

};
#endif // WMS_H
