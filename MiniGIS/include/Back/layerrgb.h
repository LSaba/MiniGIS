#ifndef LAYERRGB_H
#define LAYERRGB_H

#include "glm/glm.hpp"
#include "gdal_priv.h"
#include <vector>
#include <iostream>
#include "../../include/Back/layer.h"
#include "../../include/Back/layerraster.h"
#include <uchar.h>
#include <QImage>

class LayerRGB  : public LayerRaster
{
public:
    LayerRGB(std::string path, std::string layerName);
    void fetchRaster(std::string path);
    void createDataCoordinates();
    void createTexture(std::string path);
    void createIndexTable(std::string path);

private:
    std::string path;
    std::string layerName;
    GDALDataset *geotiffData; // data in the GDALDataset format
};

#endif // LAYERRGB_H

