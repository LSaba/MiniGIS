#ifndef VECTORGEOMETRY_H
#define VECTORGEOMETRY_H


#include "gdal_priv.h"
#include <ogrsf_frmts.h>
#include <ogr_geometry.h>
#include "gdal/gdal.h"
#include "gdal/cpl_string.h"
#include "glm/glm.hpp"


#include <iostream>
#include <regex>
#include <string>
#include <vector>

/**
 * @brief Enum of available geometry types.
 *
 */
enum class Type {
    POINT,
    LINE,
    POLYGON,
    NULLTYPE
};


/**
 * @brief Feature object easily usable by display methods.
 *
 */
struct Feature
{
    /**
     * @brief Geometry type of Feature.
     *
     */
    Type type;

    /**
     * @brief data ready to be inserted into the VertexBuffer.
     *
     */
    std::vector<glm::vec3> geometries;

    /**
     * @brief toString method for debug purposes.
     *
     * @return a string summary of the Feature.
     *
     */
    std::string typeString();
};


/**
 * @brief Interpretes the OGR/GDAL Layer object into easily usable object for display methods.
 *
 */
struct LayerFeatures
{
    /**
     * @brief The list of the layer's features.
     *
     */
    std::vector<Feature> features;

    /**
     * @brief The bounding box of the layer.
     *
     */
    std::vector<float> bbox;

    /**
     * @brief Coordonates system of the layer.
     *
     */
    int  oSRS=0;
    int getSRS(){return oSRS;}
    void setSRS(int _oSRS){
        oSRS=_oSRS;
    }

    /**
     * @brief Changes the layer's coordonates system.
     *
     * @param oTSRS the SRS that we want the data to be in.
     *
     */
    void changeSRS(int oTSRS){
        if (oSRS==0){
            throw std::logic_error("Define the Source SRS.");
        } else {
            OGRCoordinateTransformation *poCT;
            OGRSpatialReference oSourceSRS, oTargetSRS;
            oSourceSRS.importFromEPSG(oSRS);
            oTargetSRS.importFromEPSG(oTSRS);
            poCT = OGRCreateCoordinateTransformation(&oSourceSRS, &oTargetSRS);
            for (int i =0; i<features.size(); i++){
                std::vector<glm::vec3> geometries = features.at(i).geometries;
                std::vector<glm::vec3> new_geometries;
                for (int j=0; j<geometries.size(); j++){
                    double x= geometries.at(j).x;
                    double y= geometries.at(j).y;
                    double z= geometries.at(j).z;
                    if( poCT == NULL || !poCT->Transform( 1, &x, &y,&z ) )
                        printf( "Transformation failed.\n" );
                    else
                    {
                        glm::vec3 new_coord(x,y,z);
                        new_geometries.push_back(new_coord);

                        //printf( "Transformation done.\n" );
                    }
            }
            features.at(i).geometries = new_geometries;
            }
        }
    }
};


/**
 * @brief Class to process vector type geometries.
 *
 */
class VectorGeometry{

public:

    /**
     * @brief Class constructor.
     *
     */
    VectorGeometry();
    ~VectorGeometry();

    /**
     * @brief toString method for debug purposes.
     *
     * @param type Geometry type of the object.
     *
     * @return String summary of the Geometry.
     *
     */
    std::string typeString(Type type);

    /**
     * @brief Computes the data that will be inserted into the VertexBuffer for a Point type object.
     *
     * @param geometry Geometry of the Point type object.
     *
     * @return Data that will be inserted into the VertexBuffer
     *
     */
    std::vector<glm::vec3> calculatePointVertex(OGRGeometry *geometry);

    /**
     * @brief Computes the data that will be inserted into the VertexBuffer for a Line type object.
     *
     * @param geometry Geometry of the Line type object.
     *
     * @return Data that will be inserted into the VertexBuffer
     *
     */
    std::vector<glm::vec3> calculateLineVertex(OGRGeometry* geometry);

    /**
     * @brief Computes the data that will be inserted into the VertexBuffer for a Polygon type object.
     *
     * @param geometry Geometry of the Polygon type object.
     *
     * @return Data that will be inserted into the VertexBuffer
     *
     */
    std::vector<glm::vec3> calculatePolygonVertex(OGRGeometry* geometry);
};


#endif // VECTORGEOMETRY_H




