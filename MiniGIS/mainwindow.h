#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QObject>
#include "include/Front/layerspanel.h"
#include "include/Back/project.h"
#include "Front/infosfeaturespanel.h"

class LayersPanel;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow{
    Q_OBJECT

    protected:
        void keyPressEvent(QKeyEvent *event);
        void mousePressEvent(QMouseEvent *e);
        bool eventFilter(QObject *o, QEvent *e);

    // functions called when you press a button/import
    public slots:
        /**
         * @brief When you push the button, it zooms in on center of the scene
         */
        void zoomplus();

        /**
         * @brief When you push the button, it zooms out of center of the scene
         */
        void zoomminus();

        /**
         * @brief When you want to import a Vector layer to the project, you test if the path is correct. If
         * it is, you add a checkbox and switch the layer to visible on the project.
         * @see addVector(this, project)
         * @see visibleLayer(int)
         */
        void addVectorPath();

        /**
         * @brief When you want to import a Raster layer to the project, you test if the path is correct. If
         * it is, you add a checkbox and switch the layer to visible on the project.
         * @see addRaster(this, project)
         * @see visibleLayer(int)
         */
        void addRasterPath();

        void addMNTPath();

        /**
         * @brief When you want to connect to a WFS Flow, it will test if the vector (name and/or id) you give to
         * the function is empty or not. If it is, an error popup will be shown. Otherwise, it will add a
         * checkbox of the WFS with its name and its id.
         * @see addWFS(this, project)
         * @see visibleLayer(int)
         */
        void addWFSPath();

        /**
         * @brief When you want to connect to a WMS Flow, it will test if the vector (name and/or id) you give to
         * the function is empty or not. If it is, an error popup will be shown. Otherwise, it will add a
         * checkbox of the WMS with the name you gave and its id.
         * @see addWMS(this, project)
         * @see visibleLayer(int)
         */
        void addWMSPath();

        /**
         * @brief When you want to import a CityGML layer to the project, you test if the path is correct. If
         * it is, you add a checkbox and switch the layer to visible on the project.
         */
        void addCityGMLPath();

        /**
         * @brief If the checkbox is checked, the layer is set to visible in the project and not hidden. Otherwise,
         * the layer is hidden and not visible.
         * @param i (0 or 1) 1 : is selected/is hidden, 0 : is not selected/not hidden (visible)
         */
        void visibleLayer(int i);

        /**
         * @brief Is used to know if the project is saved. If it's not saved, the function saveAsProject() is called.
         * Otherwise, the path is saved in the function save(path) in the project.
         * @see saveAsProject()
         * @see project->save(pathPro)
         */
        void saveProject();

        /**
         * @brief Is used to save a project. The uiser has to choose a path and the function will test if it is correct.
         * If it is, it will set the projection, the path and the name of the project.
         * Otherwise, an error popup will show up because the user did not provide a valide .json file.
         */
        void saveAsProject();

        /**
         * @brief Permits to go from 2D to 3D and back. It prevents rotations in 2D.
         * @param state is true if we're in 3D, false otherwise.
         */
        void changeXdMode(bool state);

        /**
         * @brief Is used to delete the checkbox of a removed layer
         */
        // void deleteLayer();

        /**
         * @brief Is used to load a project. The user has to select a JSON file and it will load the project into
         * RuGIS (and all its layers).
         */
        void loadProject();

        /**
         * @brief It will collect the informations of a layer and display it on the info Panel
         */
        void getObjectinfo();

        /**
         * @brief It will collect the informations of each feature and display it on the info Panel
         */
        void getLayerinfo();

        /**
         * @brief centerLayer centers the view on the given layer.
         * @param id of the layer.
         */
        void centerLayer();

    public:
        MainWindow(QWidget *parent = nullptr);
        static MainWindow * getMainWinPtr();
        ~MainWindow();
        Project* project;
        LayersPanel *panel;

        /**
         * @brief Clears the info panel
         */
        void removetreeinfo();

    private:
        Ui::MainWindow *ui;
        static MainWindow * pMainWindow;
};

#endif // MAINWINDOW_H
