#include "../../include/Front/layerspanel.h"
#include <typeinfo>
#include "../../include/Back/wfs.h"
#include "../../include/Back/wms.h"

LayersPanel::LayersPanel():container(new QVBoxLayout){

}

// Add a vector layer (shp)
std::vector<QString> LayersPanel::addVector(QWidget *parent,Project *project){

    QString filePath = QFileDialog::getOpenFileName(parent, QObject::tr("Open File"),"/home/",
                                                    QObject::tr("Couches (*.shp)"));

    QString fileName = QFileInfo(filePath).baseName();

    if(!filePath.isEmpty()){
        std::string  file = filePath.toStdString();
        std::string  name = fileName.toStdString();
        uint id = project->addLayerSHP( file, name);

        return  std::vector<QString> {fileName, QString::number(id)};
    }

    return std::vector<QString> {filePath};
}

// Add a raster
std::vector<QString> LayersPanel::addRaster(QWidget *parent, Project *project){

    QString filePath = QFileDialog::getOpenFileName(parent, QObject::tr("Open File"),"/home/",
                                                    QObject::tr("Couches (*.tif *.tiff *.geotiff)"));

    QString fileName = QFileInfo(filePath).baseName();

    if(!filePath.isEmpty()){
        std::string  file = filePath.toStdString();
        std::string  name = fileName.toStdString();
        uint id = project->addLayerTIFF( file, name);


        return std::vector<QString> {fileName, QString::number(id)};
    }
    return std::vector<QString> {filePath};
}


// Ajouter un MNT
std::vector<QString> LayersPanel::addMNT(QWidget *parent, Project *project){

    QString filePathmnt = QFileDialog::getOpenFileName(parent, QObject::tr("Open Digital Elevation Model file"),"/home/",
                                                    QObject::tr("Couches (*.tif *.tiff *.geotiff)"));



    if(!filePathmnt.isEmpty()){
        QString filePathtex = QFileDialog::getOpenFileName(parent, QObject::tr("Open Texture File"),"/home/",
                                                        QObject::tr("Couches (*.tif *.tiff *.geotiff)"));

        QString fileNametex = QFileInfo(filePathtex).baseName();

        if(!filePathtex.isEmpty()){
            std::string  filemnt = filePathmnt.toStdString();
            std::string  filetex = filePathtex.toStdString();
            std::string  name = fileNametex.toStdString();
            uint id = project->addLayerMNT( filemnt,filetex, name);
            return std::vector<QString> {fileNametex, QString::number(id)};
        }
        return std::vector<QString> {filePathtex};

    }
    return std::vector<QString> {filePathmnt};



}


// Add a WFS flux
std::vector<QString> LayersPanel::addWFS(QWidget *parent,Project *project)
{

    QString filePath = QInputDialog::getText(parent, QObject::tr("Add a WFS URL"),
                                                    QObject::tr("URL : "),
                                                    QLineEdit::Normal);
    if(!filePath.isEmpty()){

        std::string filepath = filePath.toStdString();

        Wfs wfs = Wfs(filepath);

        std::vector<std::string> listNames = wfs.getLayerNames();
        QStringList items;
        QString empty = QString::fromStdString("");
        items << empty;

        for (int i=0; i<listNames.size();i++){
            std::string name = listNames.at(i);
            items << QString::fromStdString(name);//QObject::tr(name);
        }

        QString fileName = QInputDialog::getItem(parent, QObject::tr("Choose a Layer"),
                                                 QObject::tr("Name : "), items);

            if(!fileName.isEmpty()){
                std::string  file = filePath.toStdString();
                std::string  name = fileName.toStdString();
                for(int i=0;i<listNames.size();i++){
                    if(name==listNames[i]){
//                        std::cout<<file;
//                        std::cout<<name;
                        uint id = project->addLayerWFS(file, name);

                        return  std::vector<QString> {fileName, QString::number(id)};
                    }
                }

                return std::vector<QString> {empty};
            }

        return std::vector<QString> {filePath, fileName};
        }
    return std::vector<QString> {filePath};
}


// Add a WMS flux
std::vector<QString> LayersPanel::addWMS(QWidget *parent,Project *project){
    QString filePath = QInputDialog::getText(parent, QObject::tr("Add a WMS URL"),QObject::tr("URL : "),QLineEdit::Normal);
    if(!filePath.isEmpty()){
        QString fileName = QInputDialog::getText(parent, QObject::tr("Choose a Layer"), QObject::tr("Name : "), QLineEdit::Normal);
        if(!fileName.isEmpty()){
            std::string wmsUrl = filePath.toStdString();
            std::string wmsName = fileName.toStdString();
//            std::cout<<file;
//            std::cout<<name;
//            uint id = project->addLayerWMS(file, name);
//            string wmsUrl= "https://wxs.ign.fr/essentiels/geoportail/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIXSET=PM&TILEMATRIX=14&TILECOL=8180&TILEROW=5905&STYLE=normal&FORMAT=image/jpeg";
//            string wmsName = "test";

            uint id = project->addLayerWMS(wmsUrl, wmsName);

            return std::vector<QString> {fileName, QString::number(id)};
        }
        return std::vector<QString> {QString::fromStdString("")};
    }
    return std::vector<QString> {filePath};
}


// Add a CityGML file
std::vector<QString> LayersPanel::addCityGML(QWidget *parent,Project *project){

    QString filePath = QFileDialog::getOpenFileName(parent, QObject::tr("Open File"),"/home/",
                                                    QObject::tr("Couches (*.gml)"));

    QString fileName = QFileInfo(filePath).baseName();

    int srsEntree = QInputDialog::getInt(parent, QObject::tr("Give the layer's CRS"),
                                         QObject::tr("Enter the entering layer's CRS :"),0, 0, 10000, 1);

    if(srsEntree != 0){
        if(!filePath.isEmpty()){
            std::string  file = filePath.toStdString();
            std::string  name = fileName.toStdString();
            uint id = project->addLayerGML(file, name, srsEntree);
            return  std::vector<QString> {fileName, QString::number(id)};
        }

        return std::vector<QString> {filePath};
    }
    QString empty = QString::fromStdString("");
    return std::vector<QString> {empty};
}

// Display features

void display(Feature feat){
    Type type = feat.type;
    std::vector<glm::vec3> geom = feat.geometries;
    switch(type){
        case Type::POINT:
//            std::cout<<"Point"<<"\n";
            glBegin(GL_POINTS);
//            std::cout<<geom.size()<<std::endl;
            for(int i=0; i<geom.size(); i++){
                glVertex3f(geom[i][0], geom[i][1], geom[i][2]);
                //std::cout<<geom[i][0]<<geom[i][1]<<geom[i][2]<<std::endl;
            }
            glEnd();
//            std::cout<<"END"<<"\n";
            break;

        case Type::LINE:
//            std::cout<<"Line"<<"\n";
            glBegin(GL_LINE_STRIP);
//            std::cout<<geom.size()<<std::endl;
            for(int i=0; i<geom.size(); i++){
                glVertex3f(geom[i][0], geom[i][1], geom[i][2]);
//                std::cout<<geom[i][0]<<geom[i][1]<<geom[i][2]<<std::endl;
            }
            glEnd();
//            std::cout<<"END"<<"\n";
            break;
        case Type::POLYGON:
//            std::cout<<"Polygon"<<"\n";
            glBegin(GL_POLYGON);
//            std::cout<<geom.size()<<std::endl;
            for(int i=0; i<geom.size(); i++){
                glVertex3f(geom[i][0], geom[i][1], geom[i][2]);
//                std::cout<<geom[i][0]<<geom[i][1]<<geom[i][2]<<std::endl;
            }
            glEnd();
//            std::cout<<"END"<<"\n";
            break;
    }
}

// display a LayerFeatures
void display(LayerFeatures &vector){
    std::vector<Feature> features = vector.features;
    for(int i=0; i<features.size(); i++){
        //std::cout<<"fct";
        display(features[i]);
    }
}


//display a Raster
void display2D(std::vector<glm::vec3> data,std::vector<glm::vec2> indices, QImage tiffimage ){

    QOpenGLTexture *texture = new QOpenGLTexture(tiffimage.mirrored());

    // Render with texture
    texture->bind();

    glEnable(GL_TEXTURE_2D);

    glBegin (GL_TRIANGLES);
    for(int i=0; i<data.size(); i++){

        glTexCoord2f (indices[i][0], indices[i][1]);
        glVertex2f(data[i][0], data[i][1]);

    }
    glEnd ();
    glDisable(GL_TEXTURE_2D);

}


void display3D(std::vector<glm::vec3> data,std::vector<glm::vec2> indices, QImage tiffimage ){


    QOpenGLTexture *texture = new QOpenGLTexture(tiffimage.mirrored());

    // Render with texture
    texture->bind();

    glEnable(GL_TEXTURE_2D);

    glBegin (GL_TRIANGLES);
    for(int i=0; i<data.size(); i++){

        glTexCoord2f (indices[i][0], indices[i][1]);
        glVertex3f(data[i][0], data[i][1], data[i][2]);


    }
    glEnd ();
    glDisable(GL_TEXTURE_2D);



}

