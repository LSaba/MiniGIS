#include "../../include/Back/vectorGeometry.h"
#include <glm/gtx/string_cast.hpp>

struct Feature Feature;


VectorGeometry::VectorGeometry(){}

VectorGeometry::~VectorGeometry(){}


std::string VectorGeometry::typeString(Type type){
    if (type == Type::POINT)
        return "Point";
    else if (type == Type::LINE)
        return "Line";
    else if (type == Type::POLYGON)
        return "Polygon";
    return "NULLTYPE";
}

std::vector<glm::vec3> VectorGeometry::calculatePointVertex(OGRGeometry *geometry){
     //Get point data
     OGRPoint* point = (OGRPoint*) geometry;
     std::vector<glm::vec3> geometries;
     //Get coordinates
     double x = point->getX();
     double y = point->getY();
     double z = point->getZ();

     //Add vertex of the point to the list
     geometries.push_back(glm::vec3(x, y, z));
//       std::cout<<glm::to_string(glm::vec3(x, y, z));

     return geometries;
}


std::vector<glm::vec3> VectorGeometry::calculateLineVertex(OGRGeometry* geometry){
    OGRLineString *poRing = (OGRLineString*)geometry;
     // Access line string nodes for example :
     int numNode = poRing->getNumPoints();
     OGRPoint p;

     std::vector<glm::vec3> geometries;

     for(int k = 0; k < numNode; k++){
         poRing->getPoint(k, &p);
         //Get coordinates
         double x = p.getX();
         double y = p.getY();
         double z = p.getZ();

        //Add vertex of the point to the list
         geometries.push_back(glm::vec3(x, y, z));
     }

     return geometries;
}


std::vector<glm::vec3> VectorGeometry::calculatePolygonVertex(OGRGeometry* geometry){

     OGRPolygon  *poPoly = (OGRPolygon*) geometry;
     int numNode = (poPoly->getExteriorRing())->getNumPoints();
     OGRPoint p;

     std::vector<glm::vec3> geometries;
     for(int k = 0; k < numNode ; k++){
         //Get coordinates
         (poPoly->getExteriorRing())->getPoint(k, &p);
         double x = p.getX();
         double y = p.getY();
         double z = p.getZ();
         geometries.push_back(glm::vec3(x, y, z));
//         std::cout<<glm::to_string(glm::vec3(x, y, z));
     }

     return geometries;
 }

std::string Feature::typeString(){
    if (type == Type::POINT)
        return "Point";
    else if (type == Type::LINE)
        return "Line";
    else if (type == Type::POLYGON)
        return "Polygon";
    return "NULLTYPE";
}
