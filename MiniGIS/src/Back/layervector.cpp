#include "../../include/Back/layervector.h"
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include <ogrsf_frmts.h>
#include "../../include/Back/vectorGeometry.h"


LayerVector::LayerVector(std::string path, std::string layerName ):Layer(path.c_str(), layerName.c_str())
{
    VectorGeometry vg;
}

void LayerVector::computeGeometries()
{
    GDALAllRegister();

//    std::cout<< path << " " << layerName << "\n";
    std::vector<glm::vec3> geometries;
    Type typeGeom;
    Feature feature;
    std::vector<Feature> Listfeatures;

     GDALDataset *poDS = static_cast<GDALDataset*>(GDALDataset::Open( path.c_str(), GDAL_OF_VECTOR));
     if( poDS == nullptr )
     {
         printf( "Open failed. \n" );
         exit( 1 );
     }


     OGRLayer  *poLayer = poDS->GetLayerByName( name.c_str() );
     OGRFeatureDefn *poFDefn = poLayer->GetLayerDefn();

     poLayer->ResetReading();
     OGRFeature *poFeature;

     while( (poFeature = poLayer->GetNextFeature()) != NULL )
     {
         OGRGeometry *geometry = poFeature->GetGeometryRef();

         if( geometry != NULL )
         {
             OGRwkbGeometryType type = wkbFlatten(geometry->getGeometryType());

             std::vector<glm::vec3> geometries;

             switch(type)
             {
                case wkbPoint:
                {
                    geometries = this->vg.calculatePointVertex(geometry);
                    typeGeom = Type::POINT;

                    break;
                }
                case wkbMultiPoint:
                {
                     //Get point data
                     OGRMultiPoint* multiPoint = (OGRMultiPoint*) geometry;
                     OGRGeometry* geompoint = multiPoint->getGeometryRef(0);

                     geometries = this->vg.calculatePointVertex(geompoint);
                     typeGeom = Type::POINT;

                     break;
                }
                case wkbLineString:
                {
                     typeGeom = Type::LINE;
                     geometries = this->vg.calculateLineVertex(geometry);
//                     std::cout<<"wkbLineString";

                     break;
                }
                case wkbMultiLineString:
                {
                    //Get number of geometry (ex nb of lines)
                    OGRMultiLineString* multiline = (OGRMultiLineString*) geometry;

                    int nbGeometry = multiline->getNumGeometries();

                    for(int k = 0; k < nbGeometry; k++){
                        OGRGeometry* geomLine = multiline->getGeometryRef(k);
                        geometries = this->vg.calculateLineVertex(geomLine);
                    }

                    typeGeom = Type::LINE;

                    break;
                 }
                 case wkbPolygon:
                 {
                    geometries = this->vg.calculatePolygonVertex(geometry);
                    typeGeom = Type::POLYGON;


                    break;
                 }
                 case wkbMultiPolygon:
                 {
                     //Get number of geometry (ex nb of polygons)
                     OGRMultiPolygon* multipolygon = (OGRMultiPolygon*) geometry;

                     int nbGeometry = multipolygon->getNumGeometries();

                     for(int k = 0; k < nbGeometry; k++){
                         OGRGeometry* geomPolygon = multipolygon->getGeometryRef(k);
                         geometries = this->vg.calculatePolygonVertex(geomPolygon);
                     }

                     typeGeom = Type::POLYGON;

                     break;
                 }
                 default:
                     std::cout <<"aucun des types traités";
            }
             feature.geometries = geometries;
             feature.type = typeGeom;
             Listfeatures.push_back(feature);
         }

         OGRFeature::DestroyFeature( poFeature );
    }
      GDALClose( poDS );

     this->setLayerFeature(Listfeatures);
     
}

vector<string> LayerVector::getFieldNames()
{
    GDALAllRegister();

    std::vector<string> result;

    OGRFeature *poFeature;

    Type typeGeom;

    GDALDataset *poDS = static_cast<GDALDataset*>(GDALDataset::Open( path.c_str(), GDAL_OF_VECTOR));
    OGRLayer  *poLayer = poDS->GetLayerByName( name.c_str() );

    while( (poFeature = poLayer->GetNextFeature()) != NULL ){
         if( poLayer != NULL )
         {
             OGRFeatureDefn *poFDefn = poLayer->GetLayerDefn();
             for( int iField = 0; iField < poFDefn->GetFieldCount(); iField++ )
             {
                 OGRFieldDefn* field = poFDefn->GetFieldDefn(iField);

                 result.push_back( field->GetNameRef());

                 cout << "\n";

            }
             break;
         }
    }
    return result;

}


vector<vector<string>> LayerVector::getAttributes()
{
    GDALAllRegister();

    std::vector<std::vector<string>> result;


    OGRFeature *poFeature;

    Type typeGeom;

    GDALDataset *poDS = static_cast<GDALDataset*>(GDALDataset::Open( path.c_str(), GDAL_OF_VECTOR));
    OGRLayer  *poLayer = poDS->GetLayerByName( name.c_str() );

    while( (poFeature = poLayer->GetNextFeature()) != NULL ){
         if( poLayer != NULL )
         {

             OGRFeatureDefn *poFDefn = poLayer->GetLayerDefn();

             result.push_back(std::vector<string>{});

             for( int iField = 0; iField < poFDefn->GetFieldCount(); iField++ )
             {

                 const char* values = poFeature->GetFieldAsString(iField);

                 result[result.size()-1].push_back(string(values));

            }

         }
    }

    return result;

}

vector<string> LayerVector::getAttributesID(long IID)
{
    GDALAllRegister();

   std::vector<string> result;


    OGRFeature *poFeature;

    Type typeGeom;

    GDALDataset *poDS = static_cast<GDALDataset*>(GDALDataset::Open( path.c_str(), GDAL_OF_VECTOR));
    OGRLayer  *poLayer = poDS->GetLayerByName( name.c_str() );

    while( (poFeature = poLayer->GetNextFeature()) != NULL ){
         if( poLayer != NULL)
         {

             OGRFeatureDefn *poFDefn = poLayer->GetLayerDefn();


             if(IID==poFeature->GetFID()){

                 for( int iField = 0; iField < poFDefn->GetFieldCount(); iField++ )
                 {

                     const char* values = poFeature->GetFieldAsString(iField);

                     result.push_back(string(values));

                }
             }
         }
    }

    return result;
}

vector<float> LayerVector::getBoundingBoxFeature(uint index)
{
    LayerFeatures LF = getLayerFeatures();
    Feature Feat = LF.features.at(index);
    vector<glm::vec3>geometries = Feat.geometries;
    float MinX = geometries.at(0).x;
    float MinY = geometries.at(0).y;
    float MaxX = geometries.at(0).x;
    float MaxY = geometries.at(0).y;
    for (int i=0; i<geometries.size(); i++)
    {
        if (geometries.at(i).x<MinX){MinX = geometries.at(i).x;}
        if (geometries.at(i).y<MinY){MinY = geometries.at(i).y;}
        if (geometries.at(i).x>MaxX){MaxX = geometries.at(i).x;}
        if (geometries.at(i).y>MaxY){MaxY = geometries.at(i).y;}
    }
    return vector<float>{MinX, MaxX, MinY, MaxY};
}
vector<float> LayerVector::getBoundingBoxLayer()
{
    std::vector<Feature> LFs = this->getLayerFeatures().features;

    Feature Feat = LFs.at(0);

    float MinX = (LFs.at(0).geometries.at(0)).x;
    float MinY = (LFs.at(0).geometries.at(0)).y;
    float MaxX = (LFs.at(0).geometries.at(0)).x;
    float MaxY = (LFs.at(0).geometries.at(0)).y;
    for (uint index=0; index<LFs.size() ; index++)
    {
        Feature Feat = LFs.at(index);
        vector<glm::vec3>geometries = Feat.geometries;
        for (int i=0; i<geometries.size(); i++)
        {
            if ((LFs.at(index).geometries.at(i)).x<MinX){MinX = (LFs.at(index).geometries.at(i)).x;}
            if ((LFs.at(index).geometries.at(i)).y<MinY){MinY = (LFs.at(index).geometries.at(i)).y;}
            if ((LFs.at(index).geometries.at(i)).x>MaxX){MaxX = (LFs.at(index).geometries.at(i)).x;}
            if ((LFs.at(index).geometries.at(i)).y>MaxY){MaxY = (LFs.at(index).geometries.at(i)).y;}
        }

    }
    return vector<float>{MinX, MaxX, MinY, MaxY};
}
