#include "../../include/Back/project.h"
#include "../../include/Back/layervector.h"
#include "../../include/Back/layermnt.h"
#include "../../include/Back/layerrgb.h"
#include "../../include/Back/layerGML.h"
#include "../../include/Back/vectorGeometry.h"
#include "../../include/Back/wms.h"


Project::Project()
{
    this->layerNombre = 0;
}


Project::Project(std::string path)
{
    this->path = path;
    this->layerNombre = 0;
    static const std::regex jsonextension( ".*\\.json$");
    std::string pathstr(path);
    if(regex_match(pathstr, jsonextension)){
        ifstream file(path);
        Json::Value settings;

        file >> settings;

        Json::Value nameValue = settings["name"];
        Json::Value projectionValue = settings["projection"];
        Json::Value layersValue = settings["layers"];

        name = nameValue.asString();
        projection = projectionValue.asUInt();

//        std::cout << layersValue.size() << "\n";

        for ( int i=1; i <=layersValue.size(); i++) {

                     uint layerID = i;
                     string layerIDstr = to_string(layerID);



                     string name = layersValue[layerIDstr]["name"].asString();
                     string layerpath = layersValue[layerIDstr]["path"].asString();
                     string formatstr = layersValue[layerIDstr]["format"].asString();

//                     cout<< layerID<<"\n";

                     if (formatstr=="SHP"){
                         addLayerSHP(layerpath,name);
                     } else if (formatstr=="TIFF") {
                         addLayerTIFF(layerpath,name);
                     } else if (formatstr=="WFS") {
                         addLayerWFS(layerpath,name);
                     }
//                     //else if (formatstr=="WMS") {
//                     addLayerWMS(layerpath,name);
//                     } //else if (formatstr=="GML") {
//                     addLayerGML(layerpath,name);
//                     }

            }
         } else {
               throw "Error: Please provide a valid path to json file";
           }

}

Project::~Project() {
    layers.clear();
}


void Project::save(){

    if( sizeof(path) == 0 ) {
        throw "Error saving: please provide a path for project.";
    } else {
        save(path);
    }

}


void Project::save(std::string path){

    static const std::regex jsonextension( ".*\\.json$");
    std::string pathstr(path);
    if(regex_match(pathstr, jsonextension)){

    const char * char_path = path.c_str();
    remove(char_path);
    fstream file(path);
    file.open(path, ios::out);


    Json::Value data;
    Json::StyledStreamWriter writer;

    data["name"]=name;
    data["projection"]=projection;

    vector<uint> keys = extract_keys();

    for (int j=0; j < keys.size(); j++) {

        int i = keys[j];

        Json::Value layerParameters;

        std::string layerProjection = layers[i].getProjection();
        bool layerHidden = layers[i].getHidden();
        bool layerSelected = layers[i].getSelected();
        std::string name = layers[i].getName();
        std::string path = layers[i].getPath();
        const uint layerID = layers[i].getId();
        string layerIdstr = to_string(layerID);
        Format format = layers[i].format;
        string formatstr;

                switch (format)
                    {
                        case Format::SHP: {formatstr= "SHP"; break;};
                        case Format::TIFF:{ formatstr= "TIFF"; break;};
                        case Format::WFS: {formatstr= "WFS"; break;};
                        case Format::WMS:{ formatstr= "WMS"; break;};
                        case Format::MNT:{ formatstr= "MNT"; break;};
                        default: throw std::invalid_argument("Unimplemented item");
                    }

                layerParameters["name"]=name;
                layerParameters["path"]=path;
                layerParameters["projection"]=layerProjection;
                layerParameters["hidden"]=layerHidden;
                layerParameters["selected"]=layerSelected;
                layerParameters["format"]=formatstr;


        data["layers"][layerIdstr]=layerParameters;
    }

    writer.write(file, data);
    file.close();

    } else {
        throw "Error: Please provide a valid path to json file";
    }
}


void Project::load(std::string path)

{
    this->path = path;
    this->layerNombre = 0;
    static const std::regex jsonextension( ".*\\.json$");
    std::string pathstr(path);
    if(regex_match(pathstr, jsonextension)){
        ifstream file(path);
        Json::Value settings;

        file >> settings;

        Json::Value nameValue = settings["name"];
        Json::Value projectionValue = settings["projection"];
        Json::Value layersValue = settings["layers"];

        name = nameValue.asString();
        projection = projectionValue.asUInt();

//        std::cout << layersValue.size() << "\n";

        for ( int i=1; i <=layersValue.size(); i++) {

                     uint layerID = i;
                     string layerIDstr = to_string(layerID);



                     string name = layersValue[layerIDstr]["name"].asString();
                     string layerpath = layersValue[layerIDstr]["path"].asString();
                     string formatstr = layersValue[layerIDstr]["format"].asString();

//                     cout<< layerID<<"\n";

                     if (formatstr=="SHP"){
                         addLayerSHP(layerpath,name);
                     } else if (formatstr=="TIFF") {
                         addLayerTIFF(layerpath,name);
                     } else if (formatstr=="WFS") {
                         addLayerWFS(layerpath,name);
                     }
//                     //else if (formatstr=="WMS") {
////                     addLayerWMS(layerpath,name);
////                     } //else if (formatstr=="GML") {
////                     addLayerGML(layerpath,name);
////                     }

            }
         } else {
               throw std::invalid_argument("Error: Please provide a valid path to json file");
           }

}

//SHP
uint Project::addLayerSHP(std::string &path, std::string &layerName)
{
    static const std::regex shpextension( ".*\\.shp$");
    if(regex_match(path, shpextension)){

    this->layerNombre = this->counter(layerNombre);

    Layer layer( path.c_str(), layerName.c_str());
    layer.layerID = this->layerNombre;
    layer.format=Format::SHP;
    this->layers[this->layerNombre] =layer;

    LayerVector lv(layer.getPath(), layer.getName());
    lv.computeGeometries();
    LayerFeatures geoType= lv.getLayerFeatures();
    geoType.bbox = lv.getBoundingBoxLayer();
    this->layerFeatures[this->layerNombre] = geoType;

    return layer.layerID;
    } else {
        throw std::invalid_argument("Error: Please provide a valid path to .shp file");
    }

}

//GML
uint Project::addLayerGML(std::string &path, std::string &layerName, int srs)
{
    static const std::regex shpextension( ".*\\.gml$");
    if (regex_match(path, shpextension))
    {
        this->layerNombre = this->counter(layerNombre);

        Layer layer( path.c_str(), layerName.c_str());
        layer.layerID = this->layerNombre;
        layer.format=Format::GML;
        this->layers[this->layerNombre] =layer;

        LayerGML lv(layer.getPath(), layer.getName());
        lv.setCurrentSRS(srs);
        lv.changeSRS(4326);
        LayerFeatures geoType= lv.getLayerFeatures();
        geoType.bbox = lv.getBoundingBox();
        this->layerFeatures[this->layerNombre] = geoType;

        return layer.layerID;
    } else {
        throw std::invalid_argument("Error: Please provide a valid path to .gml file");
    }
}

//WFS
uint Project::addLayerWFS(std::string &path, std::string &layerName)
{
//    std::cout<< path<<endl;
    const std::regex URLreg("(http)");

    if(regex_search(path, URLreg)){

        this->layerNombre = this->counter(layerNombre);

        Layer layer( path.c_str(), layerName.c_str());
        layer.layerID = this->layerNombre;
        layer.format=Format::WFS;
        this->layers[this->layerNombre] =layer;


        Wfs wfs(layer.getPath(), layerName);
    //    std::cout<<layer.getName();
        std::vector<std::string> layerNames = wfs.getLayerNames();
        std::vector<std::string> layername0 = {layerName};

         wfs.computeGeometries(layername0);

         LayerFeatures geoType= wfs.getLayerFeatures();

         geoType.bbox = wfs.getBoundingBoxLayer();
         this->layerFeatures[this->layerNombre] = geoType;

         return layer.layerID;
      }
    else {
           throw std::invalid_argument("Error: Please provide a valid path to http link");
       }

    }

//TIFF
uint Project::addLayerTIFF(std::string &path, std::string &layerName)
{
    static const std::regex tiffextension( ".*\\.tiff$");
    static const std::regex geotiffextension( ".*\\.geotiff$");
    static const std::regex tifextension( ".*\\.tif$");

    if(regex_match(path, tiffextension) || regex_match(path, geotiffextension) || regex_match(path,tifextension)){

    this->layerNombre = this->counter(layerNombre);
    //cout << path << " " << layerName << "\n";
    LayerRGB layer( path, layerName);

    layer.layerID = this->layerNombre;
    //std::cout<<layer.layerID;
    layer.format=Format::TIFF;
    this->layers[this->layerNombre] =layer;

    layerRastersData[this->layerNombre] = layer.dataTIFF;
    layerRastersDataQimage[this->layerNombre] = layer.qtimage ;
    layerRastersIndices[this->layerNombre] = layer.indices;

    layerRastersBBox[this->layerNombre] = layer.getRasterBox();


    return layer.layerID;

    } else {
        throw std::invalid_argument("Error: Please provide a valid path to .tif, .tiff, or .geotiff file");
    }
}

uint Project::addLayerMNT(std::string &path, std::string &pathTexture, std::string &layerName)
{
    static const std::regex tiffextension( ".*\\.tiff$");
    static const std::regex geotiffextension( ".*\\.geotiff$");
    static const std::regex tifextension( ".*\\.tif$");

    if(regex_match(path, tiffextension) || regex_match(path, geotiffextension) || regex_match(path,tifextension)){

    this->layerNombre = this->counter( layerNombre );
    //cout << path << " " << layerName << "\n";
    LayerMNT layer( path, pathTexture, layerName );

    layer.layerID = this->layerNombre;
    //std::cout<<layer.layerID;
    layer.format=Format::MNT;
    this->layers[this->layerNombre] = layer;

    layerRastersData[this->layerNombre] = layer.dataTIFF;
    layerRastersDataQimage[this->layerNombre] = layer.qtimage ;
    layerRastersIndices[this->layerNombre] = layer.indices;

    layerRastersBBox[this->layerNombre] = layer.getRasterBox();

    return layer.layerID;

    } else {
        throw std::invalid_argument("Error: Please provide a valid path to .tif, .tiff, or .geotiff file");
    }
}

uint Project::addLayerWMS(std::string &path, std::string &layerName)
{

    static const std::regex URLreg("http");
    if(regex_search(path, URLreg)){
        this->layerNombre = this->counter(layerNombre);
        //cout << path << " " << layerName << "\n";
        WMS layer( path, layerName);

        layer.layerID = this->layerNombre;
        //std::cout<<layer.layerID;
        layer.format=Format::WMS;
        this->layers[this->layerNombre] =layer;

        layerRastersData[this->layerNombre] = layer.dataWMS;
        layerRastersDataQimage[this->layerNombre] = layer.qtimage ;
        layerRastersIndices[this->layerNombre] = layer.indices;


        return layer.layerID;
    }else {
        throw std::invalid_argument("Error: Please provide a valid path to https link");
    }
}


//TIFF
//uint Project::addLayerTIFF(std::string  &path, std::string &layerName)
//{
//    this->layerNombre = this->counter(layerNombre);
//     cout << path << " " << layerName << "\n";
//    LayerRaster layer( path.c_str(), layerName.c_str());
//    layer.layerID = this->layerNombre;
//    layer.format="TIFF";
//    this->layers[this->layerNombre] =layer;

//    cout << layer.layerID <<" "<< layer.getPath()<<" "<<layer.getName()<<" \n";
//    return layer.layerID;
//}
//LayerFeatures Project::getGeoms(uint idLayer)
//{
//    LayerVector layerVector(this->layers[idLayer].getPath(), this->layers[idLayer].getName());
//    LayerFeatures geomAndType = layerVector.getFeature( );
//    return geomAndType;
//}


//}
//TIFF
//uint Project::addLayerTIFF(std::string  &path, std::string &layerName)
//{
//    this->layerNombre = this->counter(layerNombre);
//     cout << path << " " << layerName << "\n";
//    LayerRaster layer( path.c_str(), layerName.c_str());
//    layer.layerID = this->layerNombre;
//    layer.format="TIFF";
//    this->layers[this->layerNombre] =layer;

//    cout << layer.layerID <<" "<< layer.getPath()<<" "<<layer.getName()<<" \n";
//    return layer.layerID;
//}
//Feature Project::getGeoms(uint idLayer)
//{
//    LayerVector layerVector(this->layers[idLayer].getPath(), this->layers[idLayer].getName());
//    std::cout<<this->layers[idLayer].getPath();
//    Feature geomAndType = layerVector.getLayerFeatures();
//    return geomAndType;
//}

uint Project::counter(int layerNombre)
{
    layerNombre++;
    return layerNombre;
}

map<uint, Layer> * Project::getLayers()
{
    return &layers;
}

vector<uint> Project::extract_keys() {
  vector<uint> retval;
  for (auto const& element : layers) {
    retval.push_back(element.first);
  }
  return retval;
}

Layer * Project::LayerByID(uint id)
{
    vector<uint> keys = extract_keys();
    if (find(begin(keys), end(keys), id) != end(keys))
    {
        return &(layers[id]);
    }else
    {
        return nullptr;
    }
}

map<uint, Layer> Project::getSelectedLayers()
{
    map<uint, Layer> res;
    vector<uint> keys = extract_keys();
    for(int i=0; i<keys.size(); i++)
    {
        if (layers[keys[i]].getSelected())
        {
            res.insert(pair<uint, Layer>(keys[i], layers[keys[i]]));
        }
    }

    return res;
}

void Project::removeSelectedLayers()
{
    vector<uint> keys = extract_keys();
    for(int i=0; i<keys.size(); i++)
        {
            if (layers[keys[i]].getSelected())
            {
                removeLayer(keys[i]);
            }
        }

}
map<uint, Layer> Project::getNonHiddenLayers()
{
    map<uint, Layer> res;
    vector<uint> keys = extract_keys();
    for(int i=0; i<keys.size(); i++)
    {
        if (!layers[keys[i]].getHidden())
        {
            res.insert(pair<uint, Layer>(keys[i], layers[keys[i]]));
        }
    }

    return res;
}

void Project::removeLayer(uint id)

{
    std::map<uint,Layer>::iterator it;
    vector<uint> keys = extract_keys();
    if ((it = layers.find(id)) != layers.end())
    {
        layers.erase(it);
    }
}
