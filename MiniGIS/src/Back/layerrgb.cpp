#include "../../include/Back/layerrgb.h"

LayerRGB::LayerRGB(std::string path, std::string layerName):LayerRaster(path.c_str(), layerName.c_str())
{
    LayerRaster::openGeotiff(path);

    // initialisation of width/height/bands/(x,y)origin, pixel size
    this->widthTIFF = LayerRaster::getGeotiffSize()[0];
    this->heightTIFF = LayerRaster::getGeotiffSize()[1];
    this->numberOfBands = LayerRaster::getGeotiffSize()[2];

    this->Xsize = LayerRaster::getPixelSize()[0];
    this->Ysize = LayerRaster::getPixelSize()[1]*(-1);
    this->Xo = LayerRaster::getGeotiffOrigin()[0];
    this->Yo = LayerRaster::getGeotiffOrigin()[1]-(heightTIFF*Ysize);

    fetchRaster(path);
    createDataCoordinates();

    createTexture(path);

    createIndexTable(path);
}


void LayerRGB::fetchRaster(std::string path){
    GDALAllRegister();
    this->geotiffData = (GDALDataset *) GDALOpen(path.c_str(), GA_ReadOnly );

    std::vector<std::vector<float>> out = std::vector<std::vector<float>>(widthTIFF, std::vector<float>(heightTIFF, 0));

    GDALRasterBand  *rasterBand = this->geotiffData->GetRasterBand(1);
    float *pafScanline;
    pafScanline = (float *) CPLMalloc(sizeof(float)*this->widthTIFF*this->heightTIFF);
    // RasterIO function browse the raster bands and store the data in the pafscanline
    if(rasterBand->RasterIO(GF_Read, 0, 0, this->widthTIFF, this->heightTIFF, pafScanline, this->widthTIFF, this->heightTIFF, GDT_Float32, 0, 0) == CE_None)
    {
        // Construct a height map based on the xres and yres for each group of four dots
        for (int i = 0; i < this->heightTIFF; i++)
        {
            for (int j = 0; j < this->widthTIFF; j++)
            {
                out[j][i] = pafScanline[(i)*widthTIFF + j];
            }
        }
    } else {
        std::cout<<"Error: can't read raster bands"<<std::endl;
    }
    CPLFree(pafScanline);

    this->rawData = out;
}

void LayerRGB::createDataCoordinates(){
    // result defines a vect of GeoTIFF pixel data
    std::vector<glm::vec3> result;
/*
    for (int i = 0; i < this->rawData.size() - 1; i++)
    {
        for (int j = 0; j < this->rawData[i].size() - 1; j++)
        {

            result.push_back(glm::vec3( Xo + (float)i*Xsize       , Yo + (float)j*Ysize       , this->rawData[j][i]     ) );
            result.push_back(glm::vec3( Xo + (float)(i + 1)*Xsize , Yo + (float)j*Ysize       , this->rawData[j+1][i]   ) );
            result.push_back(glm::vec3( Xo + (float)i*Xsize       , Yo + (float)(j + 1)*Ysize , this->rawData[j][i+1]   ) );

            result.push_back(glm::vec3( Xo + (float)(i + 1)*Xsize , Yo + (float)j*Ysize       , this->rawData[j+1][i]   ) );
            result.push_back(glm::vec3( Xo + (float)i*Xsize       , Yo + (float)(j + 1)*Ysize , this->rawData[j][i+1]   ) );
            result.push_back(glm::vec3( Xo + (float)(i + 1)*Xsize , Yo + (float)(j + 1)*Ysize , this->rawData[j+1][i+1] ) );
        }
    }
    this->dataTIFF = result;*/
    result.push_back(glm::vec3(Xo,Yo+heightTIFF*Ysize,0));

    result.push_back(glm::vec3(Xo,Yo,0));

    result.push_back(glm::vec3(Xo+widthTIFF*Xsize,Yo,0));

    result.push_back(glm::vec3(Xo,Yo+heightTIFF*Ysize,0));

    result.push_back(glm::vec3(Xo+widthTIFF*Xsize,Yo,0));

    result.push_back(glm::vec3(Xo+widthTIFF*Xsize,Yo+heightTIFF*Ysize,0));

    this->dataTIFF = result;
}

void LayerRGB::createTexture(std::string path){
    GDALDataset* dataset = static_cast<GDALDataset*>(GDALOpen(QString::fromStdString(path).toLocal8Bit().data(), GA_ReadOnly));

    // Get raster image size
    int rows = heightTIFF;
    int cols = widthTIFF;
    int channels = numberOfBands;

    QImage imData = QImage(cols, rows, QImage::QImage::Format_RGBA8888);//QImage::QImage::Format_RGBA8888);
    imData.fill(QColor(0, 0, 0, 255));

     // Bands start at 1
     for (int i = 1; i <= channels; ++i)
     {
         GDALRasterBand *band = dataset->GetRasterBand(i);
         switch(band->GetColorInterpretation())
         {
         case GCI_RedBand:
             band->RasterIO(GF_Read, 0, 0, cols, rows, imData.bits(),
                            cols, rows, GDT_Byte, 4, 0);
             break;
         case GCI_GreenBand:
             band->RasterIO(GF_Read, 0, 0, cols, rows, imData.bits() + 1,
                            cols, rows, GDT_Byte, 4, 0);
             break;
         case GCI_BlueBand:
             band->RasterIO(GF_Read, 0, 0, cols, rows, imData.bits() + 2,
                            cols, rows, GDT_Byte, 4, 0);
             break;
         case GCI_AlphaBand:
             band->RasterIO(GF_Read, 0, 0, cols, rows, imData.bits() + 3,
                            cols, rows, GDT_Byte, 4, 0);
             break;
         case GCI_GrayIndex:
             band->RasterIO(GF_Read, 0, 0, cols, rows, imData.bits(),
                            cols, rows, GDT_Byte, 4, 0);

             band->RasterIO(GF_Read, 0, 0, cols, rows, imData.bits()+1,
                            cols, rows, GDT_Byte, 4, 0);

             band->RasterIO(GF_Read, 0, 0, cols, rows, imData.bits()+2,
                            cols, rows, GDT_Byte, 4, 0);

             break;
         case GCI_Undefined:
             std::cout<<"__UNDEFINED__"<<std::endl;

             break;
         }
     }

     GDALClose(dataset);
    this->qtimage = imData;
}


void LayerRGB::createIndexTable(std::string path)
{
    // index generation for the texture

    std::vector<glm::vec2> indices;
/*
    for(int i = 0; i < widthTIFF-1; i++)       // for each row a.k.a. each strip
    {
        for(int j = 0; j < heightTIFF-1; j++)      // for each column
        {
            //first triangle

            indices.push_back(glm::vec2((float)i / (float)widthTIFF, (float)j / (float)heightTIFF));
            indices.push_back(glm::vec2((float)(i + 1) / (float)widthTIFF, (float)j / (float)heightTIFF));
            indices.push_back(glm::vec2((float)i / (float)widthTIFF, (float)(j + 1) / (float)heightTIFF));

            //second triangle

            indices.push_back(glm::vec2((float)(i + 1) / (float)widthTIFF, (float)j / (float)heightTIFF));
            indices.push_back(glm::vec2((float)i / (float)widthTIFF, (float)(j + 1) / (float)heightTIFF));
            indices.push_back(glm::vec2((float)(i + 1) / (float)widthTIFF, (float)(j + 1) / (float)heightTIFF));
        }
    }*/
    indices.push_back(glm::vec2(0.0, 1.0));

    indices.push_back(glm::vec2(0.0, 0.0));

    indices.push_back(glm::vec2(1.0, 0.0));

    indices.push_back(glm::vec2(0.0, 1.0));

    indices.push_back(glm::vec2(1.0, 0.0));

    indices.push_back(glm::vec2(1.0, 1.0));

    this->indices = indices;
}
